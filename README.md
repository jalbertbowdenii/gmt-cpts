# Geological time color palettes

This repository contains color palettes for geological time for use with the 
[Generic Mapping Tools](http://gmt.soest.hawaii.edu). The color palettes are
in RGB format and follow the specifications published by the International Commission
for Stratigraphy (ICS) as published on [their website](http://www.stratigraphy.org).

Currently, the following incarnations of the geological time scale are covered:

1. GTS2012 Strat chart available from [ICS website - PDF](http://stratigraphy.org/ICSchart/ChronostratChart2012.pdf)  
2. GTS2004: PDF not available online.
3. Exxon88: The old Exxon/Haq timescale from 1988.  

The cpts are also accessible through [cpt-city](http://soliton.vm.bytemark.co.uk/pub/cpt-city/) with the direct link to the cpts being the ["Heine collection" here](http://soliton.vm.bytemark.co.uk/pub/cpt-city/heine/).

Also, check my blog [TectonicWaters](http://tectonicwaters.wordpress.com) for examples of how these color scales can be used with [GPlates](http://www.gplates.org).

Please consider contributing to the repository by helping to convert the missing sections or timescales to the repository. If you work for a for-profit organisation and you will be using this for a commercial product, you might want to think about ways to donate either money, time or knowlegde back to the international geo-community the same way you are profiting from using such 'free stuff' on the web.

This work is licensed under a [Creative Commons Attribution-ShareAlike 3.0 Unported License](http://creativecommons.org/licenses/by-sa/3.0/deed.en_US).